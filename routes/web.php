<?php

use Illuminate\Support\Facades\Route;
use app\Http\Controllers\Admin\DefaultController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('index');
// });
// Route::get('/', [DefaultController::class, 'index']);
Route::get('/', 'Admin\DefaultController@index');
Route::get('/login', 'Admin\DefaultController@login');

